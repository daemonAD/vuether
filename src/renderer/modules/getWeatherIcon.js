import weatherIcons from '@/assets/icons.json'

export default function getWeatherIcon (code) {
  if (code === 0) {
    return 'wi wi-na'
  }
  const prefix = 'wi wi-'
  let weatherIcon = weatherIcons[code].icon

  // If we are not in the ranges mentioned above, add a day/night prefix.
  if (!(code > 699 && code < 800) && !(code > 899 && code < 1000)) {
    weatherIcon = 'day-' + weatherIcon
  }
  return prefix + weatherIcon
}
