import fs from 'fs'
const {app} = require('electron').remote

export default function saveSettings (updatedSettingsData) {
  fs.writeFileSync(app.getPath('userData') + '/settings.json', JSON.stringify(updatedSettingsData), 'utf-8', (err) => {
    if (err) console.log(err)
  })
}
