import fs from 'fs'
const {app} = require('electron').remote

export default function readSettings () {
  const settingsTemplate = {
    searchedLocations: [],
    favoriteLocation: '',
    lastLocation: '',
    timeFormat: '12'
  }
  let settings
  try {
    settings = JSON.parse(fs.readFileSync(app.getPath('userData') + '/settings.json', 'utf-8'))
    return settings
  } catch (err) {
    console.log(err)
    return settingsTemplate
  }
}
