import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    currentLocation: '',
    tempUnit: 'metric',
    locationDialog: {
      select: false,
      search: false
    },
    weatherData: {
    }
  },
  mutations: {
    setCurrentLocation: (state, location) => {
      state.currentLocation = location
    },
    setLocationDialog: (state, {type, action}) => {
      if (type === 'select') {
        state.locationDialog.select = action
      } else if (type === 'search') {
        state.locationDialog.search = action
      }
    },
    storeWeatherData: (state, data) => {
      state.weatherData = data
    }
  }
})
